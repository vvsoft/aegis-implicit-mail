/*
 * SmtpEmailer : A class/library for sending SMTP messages.
 * Provided by Steaven Woyan swoyan@hotmail.com
 * This package is provided without any warranty.
 * By using this package you agree that the provider makes no guarantee of use
 * and shall not be held liable or accountable for use of this software, even if it
 * fails to work properly.
 * 
 * Parts of this package include logic/code design from PJ Naughter's C++ SMTP package
 * located at http://www.naughter.com/smpt.html.  This link is provided for documentation
 * purposes and is not meant to imply his use/acknowledgement/approval of this package.
 * 
 * This package is provided with source and is free for use for any reason except that
 * it may not be sold or re-packaged by itself.
 * 
 */
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TestEmailer
{
	/// <summary>
	/// Summary description for AttachType.
	/// </summary>
	public class AttachType : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		internal System.Windows.Forms.RadioButton attachAttachment;
		internal System.Windows.Forms.RadioButton attachInline;
		internal System.Windows.Forms.TextBox contentType;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AttachType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.attachInline = new System.Windows.Forms.RadioButton();
			this.attachAttachment = new System.Windows.Forms.RadioButton();
			this.contentType = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																																						this.attachInline,
																																						this.attachAttachment});
			this.groupBox1.Location = new System.Drawing.Point(8, 40);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(264, 96);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Attachment Location";
			// 
			// attachInline
			// 
			this.attachInline.Location = new System.Drawing.Point(8, 56);
			this.attachInline.Name = "attachInline";
			this.attachInline.Size = new System.Drawing.Size(152, 24);
			this.attachInline.TabIndex = 1;
			this.attachInline.Text = "As Inline HTML Image";
			this.attachInline.CheckedChanged += new System.EventHandler(this.attachInline_CheckedChanged);
			// 
			// attachAttachment
			// 
			this.attachAttachment.Checked = true;
			this.attachAttachment.Location = new System.Drawing.Point(8, 24);
			this.attachAttachment.Name = "attachAttachment";
			this.attachAttachment.TabIndex = 0;
			this.attachAttachment.TabStop = true;
			this.attachAttachment.Text = "As Attachment";
			// 
			// contentType
			// 
			this.contentType.Location = new System.Drawing.Point(120, 16);
			this.contentType.Name = "contentType";
			this.contentType.Size = new System.Drawing.Size(152, 20);
			this.contentType.TabIndex = 1;
			this.contentType.Text = "application/octet-stream";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 2;
			this.label1.Text = "Content Type";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(48, 152);
			this.button1.Name = "button1";
			this.button1.TabIndex = 3;
			this.button1.Text = "Ok";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(144, 152);
			this.button2.Name = "button2";
			this.button2.TabIndex = 4;
			this.button2.Text = "Cancel";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// AttachType
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(298, 205);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																																	this.button2,
																																	this.button1,
																																	this.label1,
																																	this.contentType,
																																	this.groupBox1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "AttachType";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "AttachType";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Hide();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Hide();
		}

		private void attachInline_CheckedChanged(object sender, System.EventArgs e)
		{
			if(!attachInline.Checked)
			{
				contentType.Text = "application/octet-stream";
			}
		}
	}
}
